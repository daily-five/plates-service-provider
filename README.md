# Plates Template Engine for daily-five framework

This project aims to provide integration of the Plates template engine into the [daily-five framework](https://gitlab.com/daily-five/framework)

## Table of contents
   
* [Installation](#installation)
* [Usage](#usage)
* [License](#license)
   
## Installation

Install via CLI...
```bash
composer require daily-five/plates-components
```

...or adding in composer.json
```json
{
    "require": {
        "daily-five/plates-service-provider": "^1.0"
    }
}
```
If you have permission problems at installation, try this [solution](https://gitlab.com/daily-five/framework/issues/1)
   
## Usage

Enable it in your application
```php
<?php

/** @var \DailyFive\Application|\Pimple\Container $app */
$app->register(new \DailyFive\Provider\PlatesServiceProvider(), [
    
    // add folders (optional)
    'plates.folders' => [
        'name' => 'path',
    ],
    
    // add shared data (optional)
    'plates.data.shared' => [
        'msg' => 'Hello World!',
    ],
    
    // add extensions (optional)
    'plates.extensions' => [
        
    ],
]);

echo $app['template_engine']->render('hello-world', array()); 
```


## License
   
The Plates Service Provider is licensed under the MIT license.
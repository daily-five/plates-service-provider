<?php

namespace DailyFive\Controller;

use \DailyFive\Controller\BasicController;

/**
 * Class BasicTemplateController
 * @package DailyFive\Controller
 */
class BasicTemplateController extends BasicController
{
    /**
     * @var \League\Plates\Engine
     */
    protected $template_engine;

    /**
     * Here you can store the common template data for each template
     *
     * @var array
     */
    protected $common_template_data = array();

    /**
     * BasicTemplateController constructor.
     *
     * @param \League\Plates\Engine $template_engine
     */
    public function __construct(
        \League\Plates\Engine $template_engine
    ) {
        $this->template_engine = $template_engine;
    }

    /**
     * Render the given template and returns a response
     *
     * @param string $template_name
     * @param array  $template_data
     * @param int    $status
     * @param array  $headers
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function template($template_name, array $template_data = array(), $status = 200, $headers = array())
    {
        $this->template_engine->addData($this->common_template_data);
        $content = $this->template_engine->render($template_name, $template_data);
        return $this->response($content, $status, $headers);
    }

    /**
     * Wrapper for template()
     *
     * @see \DailyFive\Controller\BasicTemplateController::template()
     *
     * @param string $template_name
     * @param array  $template_data
     * @param int    $status
     * @param array  $headers
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function view($template_name, array $template_data = array(), $status = 200, $headers = array())
    {
        return $this->template($template_name, $template_data, $status, $headers);
    }

    /**
     * Add the common template data to the store
     *
     * @param string $key
     * @param mixed  $val
     *
     * @return void
     */
    protected function addCommonTemplateData($key, $val)
    {
        $this->common_template_data[$key] = $val;
    }
}

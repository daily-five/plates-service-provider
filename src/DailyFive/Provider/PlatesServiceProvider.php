<?php

namespace DailyFive\Provider;

use DailyFive\PlatesExtensions\Components;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use League\Plates\Engine as TemplateEngine;

/**
 * Class PlatesServiceProvider
 * @package DailyFive\Provider
 */
class PlatesServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple['path.templates'] = isset($pimple['path'])
            ? $pimple['path'].'/templates'
            : null;

        $pimple['plates.folders'] = array();
        $pimple['plates.data.shared'] = array();
        $pimple['plates.extensions'] = array();
        $pimple['plates.file.extension'] = 'php';

        $pimple['plates.ext.components.max_nesting_lvl'] = 0;

        $pimple[Components::class] = function ($pimple) {
            return new Components($pimple['plates.ext.components.max_nesting_lvl']);
        };

        $pimple[TemplateEngine::class] = function ($pimple) {
            $template_engine =  new TemplateEngine($pimple['path.templates']);

            $template_engine->setFileExtension($pimple['plates.file.extension']);

            foreach ($pimple['plates.folders'] as $name => $path) {
                $template_engine->addFolder($name, $path);
            }

            foreach ($pimple['plates.extensions'] as $extension) {
                $template_engine->loadExtension($extension);
            }

            $template_engine->loadExtension($pimple['plates.ext.components']);
            $template_engine->addData($pimple['plates.data.shared']);
            return $template_engine;
        };
    }

    /**
     * @return array
     */
    public function alias()
    {
        return [
            'template_engine' => TemplateEngine::class,
            'plates.ext.components' => Components::class,
        ];
    }
}